#! /usr/bin/python
# -*- coding:utf-8 -*-
'''
Created on 2017-12-27

'''
import os,sys
import urllib2,csv,re
from random import *
from bs4 import BeautifulSoup
from datetime import date, datetime
from termcolor import colored,cprint

reload(sys)
sys.setdefaultencoding("utf-8")


def add_useragent():
    uagents = []
    uagents.append('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.124 Safari/537.36')
    uagents.append('(Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.67 Safari/537.36')
    uagents.append('Mozilla/5.0 (iPad; CPU OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A5355d Safari/8536.25')
    uagents.append('Opera/9.80 (X11; Linux i686; U; hu) Presto/2.9.168 Version/11.50')
    uagents.append('Mozilla/5.0 (Windows; U; MSIE 9.0; Windows NT 9.0; en-US)')
    uagents.append('Mozilla/5.0 (X11; Linux x86_64; rv:28.0) Gecko/20100101  Firefox/28.0')
    uagents.append('Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.116 Safari/537.36 Mozilla/5.0 (iPad; U; CPU OS 3_2 like Mac OS X; en-us) AppleWebKit/531.21.10 (KHTML, like Gecko) Version/4.0.4 Mobile/7B334b Safari/531.21.10')
    uagents.append('Mozilla/5.0 (compatible; MSIE 10.0; Macintosh; Intel Mac OS X 10_7_3; Trident/6.0)')
    return uagents

def add_bots():
    bots=[]
    bots.append('http://www.bing.com/search?q=%40&count=50&first=0')
    bots.append('http://www.google.com/search?hl=en&num=100&q=intext%3A%40&ie=utf-8')
    return bots

def header():
    cachetype = ['no-cache','no-store','max-age='+str(randint(0,10)),'max-stale='+str(randint(0,100)),'min-fresh='+str(randint(0,10)),'notransform','only-if-cache']
    acceptEc = ['compress,gzip','','*','compress;q=0,5, gzip;q=1.0','gzip;q=1.0, indentity; q=0.5, *;q=0']
    acceptC = ['ISO-8859-1','utf-8','Windows-1251','ISO-8859-2','ISO-8859-15']
    bot = add_bots()
    c=choice(cachetype)
    a=choice(acceptEc)
    http_header = {
        'User-Agent' : choice(add_useragent()),
        'Cache-Control' : c,
        'Accept-Encoding' : a,
        'Keep-Alive' : '42',
        'Host' : 'bupt.edu.cn',
        'Referer' : choice(bot)
    }

    return http_header


def readUrl(url):
    try:
        http_header = header()
        request = urllib2.Request(url,headers = http_header)
        webpage = urllib2.urlopen(url)
        content = webpage.read()

        return content
        request.close()        

    except Exception,errmg:
        print 'Error on read：%s'%errmg
        return None

def parser(page):
    data = []
    soup = BeautifulSoup(page, 'lxml') # Parse the HTML as a string
    table = soup.find('table', attrs={'class':'displayTag'})
    
    for row in table.find_all('tr'):
        data.append([val.text.encode('UTF-8').strip().replace('\t', '').replace('\n', '').replace('\r', '') for val in row.find_all('td')])

    return data

def getAvailableRoom(tab):

    template = u"[md]# {16} 校本部教学楼空闲自习室\n\n\n## 全天空闲\n- 教一楼: {0}\n- 教二楼: {1}\n- 教三楼: {2}\n- 教四楼: {3}\n\n\n## 上午空闲\n- 教一楼: {4}\n- 教二楼: {5}\n- 教三楼: {6}\n- 教四楼: {7}\n\n\n## 下午空闲\n- 教一楼: {8}\n- 教二楼: {9}\n- 教三楼: {10}\n- 教四楼: {11}\n\n\n## 晚上空闲\n- 教一楼: {12}\n- 教二楼: {13}\n- 教三楼: {14}\n- 教四楼: {15}\n\n\n[/md]"
    allDay  = []
    morning = []
    afternoon = []
    night = []

    for buildIdx in range(1,5):     # J1, J2, J3, J4         
        roomList  = []
        for t in range(len(tab)):      # 1-2, 3-4 / 5-6, 7-8 / 9-10, 11-12
            roomList.append(re.findall( '{0}-\d\d\d'.format(buildIdx),''.join(tab[t]) ))
        roomSetAll = list(set(sum(roomList, []))) # All room of Jx without dupulicate

        # Available all day
        result = []
        for ele in roomSetAll:
            if  ele in roomList[0] and  ele in roomList[1] and  ele in roomList[2] and  ele in roomList[3] and  ele in roomList[4] and  ele in roomList[5]:
                result.append(ele)

        allDay.append(list(sorted(set(result))))
        
        # Available morning 
        result = []
        for ele in roomSetAll:
            if  ele in roomList[0] and ele in roomList[1]:
                result.append(ele)

        morning.append(list(sorted(set(result))))

        # Available afternoon 
        result = []
        for ele in roomSetAll:
            if  ele in roomList[2] and ele in roomList[3]:
                result.append(ele)

        afternoon.append(list(sorted(set(result))))

        # Available night 
        result = []
        for ele in roomSetAll:
            if  ele in roomList[4] and ele in roomList[5]:
                result.append(ele)

        night.append(list(sorted(set(result))))        
    
    info = template.format(', '.join(allDay[0]),    ', '.join(allDay[1]),      ', '.join(allDay[2]),      ', '.join(allDay[3]), \
                           ', '.join(morning[0]),   ', '.join(morning[1]),     ', '.join(morning[2]),     ', '.join(morning[3]), \
                           ', '.join(afternoon[0]), ', '.join(afternoon[1]),   ', '.join(afternoon[2]),   ', '.join(afternoon[3]), \
                           ', '.join(night[0]),     ', '.join(night[1]),       ', '.join(night[2]),       ', '.join(night[3]) , str(date.today()));
    print(info)
    return info



def encode2HTMLString(content):
    return content.replace('\t', '%09').replace('\r', '%0A').replace('\n', '%0A').replace(' ', '%20')   # \n use %0A

def curlSend(title, content, board = 'test', logName = 'tmp'):
    cmd = "curl -v --header \"X-Requested-With: XMLHttpRequest\" --user-agent \"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36\" --cookie \"login-user=AmIRot; nforum[UTMPUSERID]=AmIRot; nforum[UTMPKEY]=******; nforum[UTMPNUM]=******; nforum[PASSWORD]=******; nforum[BMODE]=2; nforum[XWJOKE]=hoho;\" -d  \"content={1}\" -d \"id=0\" -d \"subject={0}\" -d \"signature=1\" https://bbs6.byr.cn/article/{2}/ajax_post.json > {3} ".format(title, encode2HTMLString(content), board, logName)
    cprint(cmd, 'green', attrs=['bold'])

    os.popen(cmd)


if __name__=='__main__':
    content = readUrl('http://jwxt.bupt.edu.cn/zxqDtKxJas.jsp')

    # Is the website updated today? or maybe not?
    # print type(content.decode('gb2312'))
    yy = int( content.decode('gb2312').encode("utf-8").split(u"年")[0][-4:] );
    mm = int( content.decode('gb2312').encode("utf-8").split(u"年")[1].split(u"月")[0] );
    dd = int( content.decode('gb2312').encode("utf-8").split(u"月")[1].split(u"日")[0] );
    print(yy,mm,dd)

    if yy!=datetime.now().year  or mm!=datetime.now().month or dd!=datetime.now().day:
        logFile = './log/{0}_PostInfo.log'.format(str(date.today()))
        with open(logFile, 'w') as log:
            log.write("Faild due to date not match!")

        sys.exit(0)

    tab = parser(content)
    info = getAvailableRoom(tab)
     

    # Send
    logFile = './log/{0}_PostInfo.log'.format(str(date.today()))
    title = '今日校本部教学楼空闲自习室/{0}'.format(str(date.today()))
    board = 'StudyShare'
    content = info

    curlSend(title=title, content=content, board=board, logName=logFile)

 



